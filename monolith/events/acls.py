import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY



def get_photo(city,state):
  headers = {"Authorization": PEXELS_API_KEY}
  params = {
    "query": city + " " + state,
    "per_page": 1
  }
  url = "https://api.pexels.com/v1/search"
  response = requests.get(url, params=params, headers=headers)
  content = json.loads(response.content)

  try:
    return {"picture_url": content["photos"][0]["src"]["original"] }

  except:
    return {"picture_url": None}





def get_weather_data(city,state):
  params = {
    "query": city + " " + state + "," + "US",
    "limit": 1,
    "appid": OPEN_WEATHER_API_KEY
  }
  url = "http://api.openweathermap.org/geo/1.0/direct?q="

  response = requests.get(url,params=params)
  content = json.loads(response.content)

  try:
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]

  except KeyError:
    return {"weather": "null"}

  params2 = {
    "lat": latitude,
    "lon": longitude,
    "appid": OPEN_WEATHER_API_KEY
  }

  url2 = "https://api.openweathermap.org/data/2.5/weather"

  response2 = requests.get(url2, params2=params2)
  content2 = json.loads(response2.content2)

  return {
    "temperature": content2["main"]["temp"],
    "weather_description": content2["weather"][0]["description"]
  }
